# Introductions to Category Theory

[Category Theory](https://en.wikipedia.org/wiki/Category_theory) was invented by mathematicians
[Samuel Eilenberg](https://en.wikipedia.org/wiki/Samuel_Eilenberg) and [Saunders Mac Lane](https://en.wikipedia.org/wiki/Saunders_Mac_Lane) in the early 1940ies "in their study of algebraic topology, with the goal of understanding the processes that preserve mathematical structure". It was initially mostly of interst
to Mathematicians who used it to find patterns and translations between various areas of mathematics. As a
result the examples of Categories given were those of interest to people with enough understanding of Mathematics to be interested in seeing patterns between the different areas.

As computing grew, it was found that the same tools could be used in programming, and then also in wider
areas of practical interest. As we are interested in Categories related to the Web, we will look at introductions that are closer to computing. 
It turns out that the when Cats is explained with examples from a domain the learner understands it is not that difficult to learn. 

## Video introductions

Learning Category theory means getting a feeling as to how one speaks about it, and how
it is used. This often involves drawing diagrams. It helps to see those being drawn as
this brings the diagrams to life.

### The MIT 2020 Lectures: Programming with Categories 

This series of lectures ([course material](http://brendanfong.com/programmingcats.html)) brings together Mathematicians David Spivak, Brendan Fong and Programmer/ex Physicist Bartosz Milewski, to explain Category Theory's application to programming.

* The introductory session presents each of the three speakers and motivates the course. 
For people coming from programming the [philosophical introduction by Milewski at min 12](http://brendanfong.com/programmingcats.html) is particularly helpful to ground interest in what follows.

<iframe width="560" height="315" src="https://www.youtube.com/embed/NUBEB9QlNCM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* In Lecture 1, David Spivak explains what a Category is, with intutions coming from Sets (though no prior knowledge is required), and ends with the definition of a Category.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3W0h3WzxgIE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* In Lecture 2, Brendan Fong illustrates the definition with categories with 1 and 2 objects, pre orders, Monoids, some ways to think about isomorphism and why these capture a concept of structure. The lambda calculus is quickly introduced and its correspondence with Haskell is shown. (A nice intro to Haskell if you want to 
dig deeper is [Learn You a Haskell for Great Good!](http://learnyouahaskell.com/))

<iframe width="560" height="315" src="https://www.youtube.com/embed/jm3bJrULMqM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* In Lecture 3, Bartosz Milewski, moves from untyped lambda calculus to typed calculus, showing how simple it is to write generic function composition in Haskell, and finishes with morphisms between categories known as functors 

<iframe width="560" height="315" src="https://www.youtube.com/embed/1ar_eC9tfm0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* The full playlist of [Programming with Categories](https://www.youtube.com/playlist?list=PLhgq-BqyZ7i7MTGhUROZy3BOICnVixETS) on YouTube.

### Further Material

The [Programming with Categories Home Page](http://brendanfong.com/programmingcats.html) lists further resources. Especially useful is

* Bartosz Milewski's ['Category Theory for Programmers'](https://github.com/hmemcpy/milewski-ctfp-pdf/) with a version illustrating the concepts using Haskell and Scala.
* Bartosz Milewski's [online courses](https://www.youtube.com/user/DrBartosz/playlists)
* Brendan and David's course on [Applied Category Theory](https://ocw.mit.edu/courses/mathematics/18-s097-applied-category-theory-january-iap-2019/) that preceeded this course and shows how Category Theory can be applied in even wider areas.

## Databases with Categories

As mentioned above, learning about categories using examples familiar to one, helps make the
ideas much easier to understand. Databases are one of the most widely used applications of 
computers. Below  are some of the papers that really make clear how close Category Theory is to 
databases.

* 2010 - David Spivak: [Functorial Data Migration](https://arxiv.org/pdf/1009.1166.pdf)
This paper starts with with the stunning claim:
>  The first goal is to present a straightforward category-theoretic model of databases under which every theorem about small categories becomes a theorem about databases
It proceeds to show this using concepts of categories, functors and natural transformations, clarifying the import of those concepts by doing so.
 
* 2012 - David Spivak and Robert Kent: [OLogs: A Categorical Framework for Knowledge Representation](https://math.mit.edu/~dspivak/informatics/olog.pdf)
A very clear explanation of how one can use Categories for an Ontology Logic, using small categories for the schemas, functors from those to Set for the instance data, and how one can transform to other schemas using natural transformations.

* The work above was also integrated into a [2013 course at MIT](http://math.mit.edu/~dspivak/teaching/sp13/) which lead to a book [Category Theory for Scientists](https://archive.org/details/cattheory) that is also a very good introduction to the subject.

The above papers do consider the semantic web standards, since these are designed to turn the Web into a decentralised Database. But Evan Patterson's 2017 paper [Knowledge Representation in Bicategories of Relations](https://www.epatters.org/assets/papers/2017-relational-ologs.pdf) shows how one can get much closer to the RDF and OWL standards by moving from the Category of Sets to the Category Rel of Sets and Relations.

See also [our list of papers on the Semantic Web](../RDF/Literature.md).

## Object Oriented Programming

Most programming languages in use are now Object Oriented, from Java, JavaScript, Python, to [Scala](https://www.scala-lang.org) which is a hybrid. Interestingly enough the best modelling of Object Oriented programming is as coalgebras, which emphasises observability over construction, statefullness and processuality. 
For those who have worked a lot in Object Oriented spaces the following papers will also be helpful.

* 1996 - Bart Jacobs: [Objects and Classes, Co-Algebraically](https://www.semanticscholar.org/paper/Objects-and-Classes%2C-Co-Algebraically-Jacobs/c7c45abf7d99e0aef627fd5223023bf82e70dc71)
* 1996 - Bart Jacobs: [Inheritance and Cofree Constructions](http://www.lirmm.fr/~ducour/Doc-objets/ECOOP/papers/1098/10980210.pdf)
* 2003 - Bart Jacobs, Erik Poll: [Coalgebras and Monads in the Semantics of Java](https://pdf.sciencedirectassets.com/271538/1-s2.0-S0304397500X04847/1-s2.0-S0304397502003663/main.pdf)
* 1997 - Bart Jacobs, Jan Rutten: [A Tutorial on (Co)Algebras and (Co)Induction](https://pdfs.semanticscholar.org/40bb/e9978e2c4080740f55634ac58033bfb37d36.pdf)
* 2017 - Bart Jacobs: [Introduction to Coalgebras](https://www.cambridge.org/core/books/introduction-to-coalgebra/0D508876D20D95E17871320EADC185C6)
A Book that gives a full view of Category Theory from the lens of Coalgebras 

