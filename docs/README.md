# Web Cats Community Group

Documentation and resources for a to be proposed [Web-Cats](https://www.w3.org/community/groups/proposed/) W3C Community Group on linking Web technologies and Category Theory. 
The aim is to bring these communities together, build up a wiki of existing research with comprehensible abstracts, and a list of questions in the [issue database](https://gitlab.com/web-cats/CG/issues).

## What are Cats?

See the [online courses](Cats/Intro.md).

## Do you have what it takes to be a Cat Herder?

Building standards for the web is hard. 
Without Maths it is even harder.
If you feel up to it, come join us in Gitter channel:

[![Zulip WebCats Chat](https://img.shields.io/badge/zulip-join_chat-brightgreen.svg)](https://categorytheory.zulipchat.com/#narrow/stream/229156-practice.3A-applied-ct) 


Gitter allows one to express mathematical formulas with [KaTeX](https://katex.org/docs/supported.html).

More on the life of Cat Herders below:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vTwJzTsb2QQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Some of the Cats we come across

* [CT interpretations of RDF](RDF/Literature.md)
* HTTP interpreted [coalgebraically](https://books.google.co.uk/books?hl=en&lr=&id=tApQDQAAQBAJ&oi=fnd&pg=PR7&dq=coalgebra+jacobs&ots=Zl1boEbV8w&sig=4IKO8zn-lMms1EKNSFU9uYeYv_8) or in terms of Categories of Games
* Query Languages formalisations: Sparql, CQL, [ShEx](https://www.w3.org/2001/sw/wiki/ShEx)
* Modal Logics, Monads, ... to allow formalisation of reasoning about access control, and perhaps find a semantics for Quads.
* [Algebraic Property Graphs](https://arxiv.org/abs/1909.04881)
* ...

## What we produce

We aim to draw a map of this space, by starting from questions. 
For a list of questions see the [issue database](https://gitlab.com/web-cats/CG/issues). 
If your question is not there, feel free to write one up.
If you know some answers, please add them.
We also hope to put together notes of accumulated wisdom. 

We have chosen GitLab to host the CG as it allows [mathematical notation in Markdown](https://docs.gitlab.com/ee/user/markdown.html#math) in the repo as well as in issues.
For example:

$\Sigma \dashv \Delta \dashv \Pi$

To edit this web site go to the [webcats gitlab group](https://gitlab.com/web-cats/web-cats.gitlab.io). 

