# Resource Description Framework  (RDF)

RDF ([wikipedia entry](https://en.wikipedia.org/wiki/Resource_Description_Framework)) is a first order logic for the web built on Graphs. See the paper by Tim Berners-Lee et al.  [N3Logic: A logical framework for the World Wide Web](https://www.cambridge.org/core/journals/theory-and-practice-of-logic-programming/article/n3logic-a-logical-framework-for-the-world-wide-web/5CB102B7E35457C8D07EC2B8281C8317). Category Theory also builds on Graphs. Indeed any Graph gives rise to the Free Category of that Graph. It is quite important thus to understand how the two are related. 

One major problem that needs to be resolved is how the [Dataset construct in RDF1.1](https://www.w3.org/TR/rdf11-mt/#rdf-datasets) that was first used by N3 and as Named Graphs in SPARQL fits in, as these seem to require something like graphs inside graphs. These don't have a formally agreed semantics. Yet, usage shows that N3 Graphs can be used for modal logic, opaque contexts, etc... hence the interest below in how Cats deal with modal logics.

## RDF and Cats papers

A number of papers address directly the relation of RDF to Cats.

* 2006,  Dorel Lucanu, Yuan Fang, LiJin Song Dong: [Semantic Web Languages – Towards an Institutional Perspective](https://link.springer.com/chapter/10.1007/11780274_6)
  This early paper used [Institution Theory](https://www.iep.utm.edu/insti-th/) to provide a general overview of how the various RDF logics fit together.

* 2005, Pascal Hitzler, Markus Krötzsch, Marc Ehrig, York Sure: [What Is Ontology Merging? – A Category-Theoretical Perspective Using Pushouts](http://people.cs.ksu.edu/~hitzler/resources/publications/cando05.pdf)

* 2006, A Zimmermann, M Krotzsch, J Euzenat, P Hitzler: [Formalizing ontology alignment and its operations with category theory](https://corescholar.libraries.wright.edu/cgi/viewcontent.cgi?article=1200&context=cse).

* 2009, PhD Thesis, Benjamin Braatz: [Formal Modelling and Application of Graph Transformations in the Resource Description Framework](https://pdfs.semanticscholar.org/b8c8/5a3e7a04020259ec9a58c7e5563033f52844.pdf). Chapter 2 of the thesis gives a faithful Category Theoretic interpretation of RDF. 

* 2010, Jie Bao, Jiao Tao, Deborah L.McGuinness [Context Representation for the Semantic Web](https://eprints.soton.ac.uk/270829/1/2010-03-25_context_websciv2.pdf).
This paper looks at how one could represent contexts using [Institution Theory](https://www.iep.utm.edu/insti-th/), a part of category theory that builds a framework to analyse all logics semantically.

* 2011, Benjamin Braatz, Ulrike Golas, Thomas Soboll: [How to delete categorically — Two pushout complement constructions](https://www.sciencedirect.com/science/article/pii/S0747717110001665)

* 2017, Evan Patterson [Knowledge Representation in Bicategories of Relations](https://www.epatters.org/assets/papers/2017-relational-ologs.pdf). 
This article shows how one gets a typed version of RDF and OWL. 
It adapts the functorial view of databases and ontologies, developed by Spivak et al in [Ologs: A categorical Framework for Knowledge Representation](https://math.mit.edu/~dspivak/informatics/olog.pdf), to the bicategory of relations, which fits RDF a lot better. This is not trying to be close to the RDF specs as such, but gives a very clear view of how Catsters would approach this space.

* 2019, Joshua Shinavier, Ryan Wisnesky: [Algebraic Property Graphs](https://arxiv.org/pdf/1909.04881.pdf).
Works on conceptually unifying Property Graphs and RDF stores.

* 2019, Dominique Duval, Rachid Echahed, Frederic Prost: [On foundational aspects of RDF and SPARQL](https://arxiv.org/abs/1910.07519)
Recent article that does not fully cover the literature, and though it looks at SPARQL does not really cover
the graphs inside of graphs aspect that one finds in N3.


## RDF, Contexts and Modal Logic

The following papers look at RDF and modal logic but without bringing Category Theory into the Picture

* 1995, PhD Thesis, [Ramanathan V Guha](https://en.wikipedia.org/wiki/Ramanathan_V._Guha): [Contexts A Formalization and Some Applications](http://www.filosoficas.unam.mx/~morado/TextosAjenos/guha-thesis.pdf).
Guha was one of the original figures behind RDF. It is interesting to see this interest in Contexts so early on.

* 2007, Pat Hayes: [Context Mereology](https://www.aaai.org/Papers/Symposia/Spring/2007/SS-07-05/SS07-05-011.pdf).

* 2013, Phd Thesis, Szymon Klarman: [Reasoning with contexts in description logics](https://www.cs.vu.nl/en/Images/dissertation_Szymon_Klarman_27-2-2013_tcm210-319714.pdf)


## Modal Logic and Cats

* 2006, Abadi: [Access Control in a Core Calculus of Dependency](https://users.soe.ucsc.edu/~abadi/Papers/acldcc-acm.pdf)
  Here Abadi updates the famous work from 1989 [A Logic of Authentication](https://royalsocietypublishing.org/doi/abs/10.1098/rspa.1989.0125) by showing how the earlier work can be recast in terms of indexed monads.

* 2011, Phd Thesis, Kohei Kishida: [Generalized Topological Semantics for First-Order Modal Logic](http://d-scholarship.pitt.edu/10224/).
 PhD reviewed by  Steve Awodey, Dana Scott, Nuel Belnap, Robert Brandom. Looks in detail at how Category Theory can help shed light on first order modal logic debates between Kripke and David Lewis. See also his article in Elaine Landry's [Categories for the Working Philosoher](https://books.google.fr/books?id=RIM8DwAAQBAJ).

* 2011, Corina Cirstea, Alexander Kurz, Dirk Pattinson, Lutz Schröder, Yde Venema: [Modal Logics are Colagebraic](https://academic.oup.com/comjnl/article-abstract/54/1/31/336864)

* 2018, David Corfield: [Modal Homotopy Type Theory](http://philsci-archive.pitt.edu/15260/).
For some background see [this blog post](https://medium.com/@bblfish/modal-hott-on-the-web-2f4f7996b41f). One fascinating quote here is

> The slogan here is that, where HoTT itself is the internal language of(∞,1)-toposes, modal HoTT is the internal language for collections of(∞,1)-toposes related by geometric morphisms

More references on the [Adjoint Logic page on nlab wiki](https://ncatlab.org/nlab/show/adjoint+logic) and links from there.

* 2019, G. A. Kavvos, [Modalities, Cohesion, and Information Flow](https://arxiv.org/abs/1809.07897) develops the idea of classified sets using adjunctions to reason about information flow security.



