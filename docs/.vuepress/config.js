module.exports = {
  title: 'Web Cats',
  description: 'W3C Web Cats Community Group',
  dest: 'public',
  plugins: [
    [
      'vuepress-plugin-mathjax',
      {
        target: 'svg',
        macros: {
          '*': '\\times',
        },
      },
    ],
  ],
}
