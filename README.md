# web-cats.gitlab.io

Documentation and resources for a [Web-Cats](https://www.w3.org/community/groups/) W3C Community Group on linking Web technologies and Category Theory. Join our discussion in Gitter here:

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/webcats/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge) 

This repository contains the public web site located at [https://web-cats.gitlab.io/](https://web-cats.gitlab.io/). It is built on [VuePress](https://vuepress.vuejs.org) and enhanced with the [mathjax plugin](https://vuepress.github.io/en/plugins/mathjax/#installation).

The front page of the site is [docs/README.md](docs/README.md).

To get this to work for gitlabs we adapted Sam Beckham's [vuepress-gl-pages](https://gitlab.com/samdbeckham/vuepress-gl-pages).

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

This sets up a `node` environment, then uses `yarn install` to install dependencies and `yarn build` to build out the website to the `./public` directory.
It also caches the `node_modules` directory to speed up sebsequent builds.

## Building locally

In order to make changes to this web site and test your changes you should install it locally.
This project uses [yarn](https://yarnpkg.com), you'll need to install this globally before you can get started.

```
npm install -g yarn
```

Then you need to install the project dependencies:

```
yarn install
```

Now you're ready to go.
To run the local dev server just use the following command:

```
yarn start
```

Your website should be available at [http://localhost:8080/](http://localhost:8080/vuepress).

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Project name
You'll need to set the correct base in docs/.vuepress/config.js.

If you are deploying to https://<USERNAME or GROUP>.gitlab.io/, you can omit base as it defaults to "/".

If you are deploying to https://<USERNAME or GROUP>.gitlab.io/<REPO>/, (i.e. your repository is at https://gitlab.com/<USERNAME>/<REPO>), set base to "/<REPO>/".

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

[ci]: https://about.gitlab.com/gitlab-ci/
[project]: https://vuepress.vuejs.org/
[install]: https://vuepress.vuejs.org/guide/getting-started.html
[documentation]: https://vuepress.vuejs.org/guide/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

